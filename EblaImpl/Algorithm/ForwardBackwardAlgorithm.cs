﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.

This class was based on the following articles and 2006-2015 Jarek Lipski - 
Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

[1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
[2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
Intelligent Information Systems, 279-286.
[3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using EblaAPI;
using EblaImpl.Calculator;
using EblaImpl.Translation;
using Jama;

namespace EblaImpl.Algorithm
{
    /// <summary>
    /// Forward-Backward algorithms
    /// </summary>
    public class ForwardBackwardAlgorithm
    {

        #region  Constants

        private const int DefaultBandRadius = 20;
        private const float DefaultBandIncrementRatio = 1.5f;
        private const int DefaultInitialBandRadius = DefaultBandRadius;
        private const int DefaultMinBandMargin = DefaultInitialBandRadius / 4;

        #endregion

        #region Properties

        private readonly List<Tuple<int, int, float>> _mooreScoreBase;
        private float BandRadius { get; set; }
        public int InitialBandRadius { get; set; }
        public float BandIncrementRatio { get; set; }
        public int MinBandMargin { get; set; }
        public ICalculator Calculator { get; set; }

        private static List<Tuple<int, int, float>> GetBestScoreBase()
        {
            return CreateBestScoreBase();
        }

        private static List<Tuple<int, int, float>> CreateBestScoreBase()
        {
            var scoreBase = new List<Tuple<int, int, float>>
            {
                new Tuple<int, int, float>(1, 1, -(float) Math.Log(0.9)),
                new Tuple<int, int, float>(1, 0, -(float) Math.Log(0.005)),
                new Tuple<int, int, float>(1, 2, -(float) Math.Log(0.045)),
                new Tuple<int, int, float>(0, 1, -(float) Math.Log(0.005)),               
                new Tuple<int, int, float>(2, 1, -(float) Math.Log(0.045))
            };
            return scoreBase;
        }

        private static List<Tuple<int, int, float>> GetMooreScoreBase()
        {
            return CreateMooreScoreBase();
        }

        private static List<Tuple<int, int, float>> CreateMooreScoreBase()
        {
            var mooreScoreBase = new List<Tuple<int, int, float>>
            {
                new Tuple<int, int, float>(1, 1, -(float) Math.Log(0.94)),
                new Tuple<int, int, float>(1, 0, -(float) Math.Log(0.01)),
                new Tuple<int, int, float>(0, 1, -(float) Math.Log(0.01)),
                new Tuple<int, int, float>(2, 1, -(float) Math.Log(0.02)),
                new Tuple<int, int, float>(1, 2, -(float) Math.Log(0.02))
            };
            return mooreScoreBase;
        }

        #endregion

        #region Constructors
        public ForwardBackwardAlgorithm() : this(DefaultBandRadius)
        {
            InitialBandRadius = DefaultBandRadius;
            BandIncrementRatio = DefaultBandIncrementRatio;
            MinBandMargin = DefaultMinBandMargin;
            _mooreScoreBase = GetBestScoreBase();
        }

        public ForwardBackwardAlgorithm(int bandRadius)
        {
            BandRadius = bandRadius;
        }

        public ForwardBackwardAlgorithm(int initialBandRadius, float bandIncrementRatio, int minBandMargin)
        {
            InitialBandRadius = initialBandRadius;
            BandIncrementRatio = bandIncrementRatio;
            MinBandMargin = minBandMargin;
        }
        #endregion Constructors

        #region Methods

        public List<BestAlignment> ForwardBackward(SegmentTokens[] baseSegmentList,
            SegmentTokens[] versionSegmentList)
        {
            BandRadius = InitialBandRadius / BandIncrementRatio;
            var maxAlignmentRadius = (int) BandRadius + 1;
            var bestAlignments = new List<BestAlignment>();
            while ((maxAlignmentRadius + MinBandMargin) > BandRadius)
            {
                BandRadius *= BandIncrementRatio;
                bestAlignments = ForwardBackward(baseSegmentList, versionSegmentList, BandRadius);

                maxAlignmentRadius = CalculateMaxAlignmentRadius(bestAlignments, baseSegmentList.Length,
                    versionSegmentList.Length);
            }
            return bestAlignments;
        }

        private static int CalculateMaxAlignmentRadius(List<BestAlignment> alignments, 
            int baseCount, int versionCount)
        {
            var maxRadius = 0;
            var baseNumber = 0;
            var versionNumber = 0;
            var versionBaseRatio = (float) versionCount / (float) baseCount;

            foreach (var alignment in alignments)
            {
                baseNumber += alignment.BaseSegmentList.Length;
                versionNumber += alignment.VersionSegmentList.Length;
                var diagonalVersion = (int) ((float) baseNumber * versionBaseRatio);
                var radius = Math.Abs(versionNumber - diagonalVersion);
                if (radius > maxRadius)
                    maxRadius = radius;
            }

            return maxRadius;
        }

        public List<BestAlignment> ForwardBackward(SegmentTokens[] baseSegmentList,
            SegmentTokens[] versionSegmentList, float bandRadius)
        {
            var forwardMatrix = Forward(baseSegmentList, versionSegmentList, bandRadius);

            var backwardMatrix = Backward(baseSegmentList, versionSegmentList, bandRadius);

            var bestAlignments = ForwardBackward(baseSegmentList, versionSegmentList, 
                forwardMatrix, backwardMatrix);

            return bestAlignments;
        }


        public List<BestAlignment> ForwardBackward(SegmentTokens[] baseSegmentList,
            SegmentTokens[] versionSegmentList, BandMatrix fwdMatrix, BandMatrix bwdMatrix)
        {
            var bestAlignments = new List<BestAlignment>();

            var totalScore = fwdMatrix.GetEquivalentValue(baseSegmentList.Length, versionSegmentList.Length);
            var col = 0;
            var row = 0;

            while (col < baseSegmentList.Length || row < versionSegmentList.Length)
            {
                var bestScore = float.PositiveInfinity;
                var bestScoreBase = new Tuple<int, int, float>(0, 0, 0);

                foreach (var scoreBase in _mooreScoreBase)
                    //.OrderBy(p => p.Item1).ThenBy(q => q.Item2))
                {
                    var newX = col + scoreBase.Item1;
                    var newY = row + scoreBase.Item2;

                    if (newX < 0 || newY < 0) continue;
                    if (newX > baseSegmentList.Length || newY > versionSegmentList.Length) continue;

                    var fwdScore = fwdMatrix.GetEquivalentValue(newX, newY);
                    var bwdScore = bwdMatrix.GetEquivalentValue(newX, newY);
                    var score = fwdScore + bwdScore - totalScore;
                    if (!(score < bestScore)) continue;
                    bestScore = score;
                    bestScoreBase = new Tuple<int, int, float>(scoreBase.Item1, scoreBase.Item2, bestScore);
                }

                var alignment = new BestAlignment();
                alignment.BaseSegmentList =
                    TranslationUtil.GetSubList(baseSegmentList, col, col + bestScoreBase.Item1).ToArray();
                alignment.VersionSegmentList =
                    TranslationUtil.GetSubList(versionSegmentList, row, row + bestScoreBase.Item2).ToArray();
                alignment.Score = bestScoreBase.Item3;

                bestAlignments.Add(alignment);

                col += bestScoreBase.Item1;
                row += bestScoreBase.Item2;
            }

            return bestAlignments;
        }

        private BandMatrix Forward(SegmentTokens[] baseSegmentList,
            SegmentTokens[] versionSegmentList, float bandRadius)
        {
            var forwardMatrix = new BandMatrix(baseSegmentList.Length + 1, versionSegmentList.Length + 1,
                (int)bandRadius);

            for (var col = 0; col < versionSegmentList.Length + 1; col++)
            {
                for (var row = 0; row < baseSegmentList.Length + 1; row++)
                {
                    var data = Forward(row, col, baseSegmentList, versionSegmentList, forwardMatrix);
                    forwardMatrix.SetEquivalentValue(row, col, data);
                }
            }
            return forwardMatrix;
        }

        internal float Forward(int x, int y, SegmentTokens[] baseSegmentList,
            SegmentTokens[] versionSegmentList, BandMatrix matrix)
        {
            var scoreList = new List<float>();

            foreach (var scoreBase in _mooreScoreBase)
            {
                var startX = x - scoreBase.Item1;
                var startY = y - scoreBase.Item2;

                if (startX < 0 || startY < 0) continue;

                var baseList = TranslationUtil.GetSubList(baseSegmentList, startX, x);
                var versionList = TranslationUtil.GetSubList(versionSegmentList, startY, y);

                float score = scoreBase.Item3 + Calculator.CalculateScore(baseList.ToArray(), versionList.ToArray());
                float totalScore = score + matrix.GetEquivalentValue(startX, startY);
                scoreList.Add(totalScore);
            }
            var scoreSum = ScoreSum(scoreList);
            return scoreSum;
        }

        private BandMatrix Backward(SegmentTokens[] baseSegmentList,
           SegmentTokens[] versionSegmentList, float bandRadius)
        {
            var backwardMatrix = new BandMatrix(baseSegmentList.Length + 1, versionSegmentList.Length + 1,
                (int)bandRadius);

            for (var col = versionSegmentList.Length; col >= 0; col--)
            {
                for (var row = baseSegmentList.Length; row >= 0; row--)
                {
                    var data = Backward(row, col, baseSegmentList, versionSegmentList, backwardMatrix);
                    backwardMatrix.SetEquivalentValue(row, col, data);
                }
            }
            return backwardMatrix;
        }

        internal float Backward(int x, int y, SegmentTokens[] baseSegmentList,
            SegmentTokens[] versionSegmentList, BandMatrix matrix)
        {
            var scoreList = new List<float>();

            foreach (var scoreBase in _mooreScoreBase)
            {
                var endX = x + scoreBase.Item1;
                var endY = y + scoreBase.Item2;

                if (endX < 0 || endY < 0) continue;
                if (!(endX <= baseSegmentList.Length && endY <= versionSegmentList.Length)) continue;
                
                var baseList = TranslationUtil.GetSubList(baseSegmentList, x, endX);
                var versionList = TranslationUtil.GetSubList(versionSegmentList, y, endY);
                var score = scoreBase.Item3 + Calculator.CalculateScore(baseList.ToArray(), versionList.ToArray());
                var totalScore = score + matrix.GetEquivalentValue(endX, endY);
                scoreList.Add(totalScore);
            }
            var scoreSum = ScoreSum(scoreList);
            return scoreSum;
        }

        internal float ScoreSum(ICollection<float> scoreList)
        {
            if (scoreList.Count == 0) return 0.0f;

            var minScore = scoreList.Min();
            if (float.IsPositiveInfinity(minScore))
                return float.PositiveInfinity;

            double probabilitySum = 0;
            foreach (var score in scoreList)
                probabilitySum += Math.Exp(-(score - minScore));
            var probabilityScore = -(float) Math.Log(probabilitySum);
            var scoreSum = minScore + probabilityScore;

            return scoreSum;
        }

        #endregion

    }


}
