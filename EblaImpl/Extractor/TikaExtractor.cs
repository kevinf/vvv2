﻿using org.apache.tika.parser;
using System;
using System.Collections.Generic;
using System.Text;
using ikvm.extensions;
using java.io;
using org.apache.tika.detect;
using org.apache.tika.metadata;
using org.apache.tika.extractor;
using javax.xml.transform.sax;
using org.apache.tika.parser.txt;
using javax.xml.transform;
using javax.xml.transform.stream;
using sun.nio.cs;
using org.apache.tika.parser.html;
using com.google.common.@base;
using HtmlAgilityPack;
using org.apache.poi.ss.formula.functions;
using org.apache.tika.sax;

namespace EblaImpl.Extractor
{
    public class TikaExtractor
    {
        private ParseContext _context;
        private Parser _parser;
        private List<EblaAPI.HtmlParseError> _parseErrors = new List<EblaAPI.HtmlParseError>();

        public TikaExtractor() : base()
        {
            _context = new ParseContext();
            var detector = new DefaultDetector();
            _parser = new AutoDetectParser(detector);

            _context.set(typeof(org.apache.tika.parser.Parser), _parser);
            _context.set(typeof(HtmlMapper), new CustomHtmlMapper());
        }

        public byte[] ConvertFileToHtmlWithEmbeddedImages(byte[] file)
        {
            var parser = new AutoDetectParser();
            var output = new ByteArrayOutputStream();
            var factory = (SAXTransformerFactory)TransformerFactory.newInstance();
            var inputStream = new ByteArrayInputStream(file);
            try
            { 
                var metaData = new Metadata();
                var encodingDetector = new UniversalEncodingDetector();

                var encode = encodingDetector.detect(inputStream, metaData) ?? new UTF_32();

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, encode.toString());
                handler.setResult(new StreamResult(output));

                var imageRewriting = new ImageRewritingContentHandler(handler);

                var context = new ParseContext();
                context.set(typeof(EmbeddedDocumentExtractor), new FileEmbeddedDocumentExtractor());

                parser.parse(inputStream, imageRewriting, new Metadata(), context);

                var array = output.toByteArray();

                return array;
            }
            catch (Exception e)
            {
                throw new Exception("Document conversion was not possible.", e);
            }
            finally
            {
                inputStream.close();
            }
        }

        public byte[] ConvertAnyFileToHtml(byte[] file)
        {
            var input = new ByteArrayInputStream(file);
            var ouput = new StringWriter();

            try
            {
                var metadata = new Metadata();
                var factory = (SAXTransformerFactory) TransformerFactory.newInstance();

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                handler.setResult(new StreamResult(ouput));

                _parser.parse(input, handler, metadata, _context);

                var extractedContent = ouput.toString();

                return extractedContent.getBytes("UTF-8");

            }
            catch (Exception e)
            {
                throw new Exception("Document conversion was not possible.", e);
            }
            finally
            {
                input.close();
            }
        }

        public string ConvertTextToHtml(string file)
        {
            var input = new ByteArrayInputStream(Encoding.UTF8.GetBytes(file));
            //var tika = new Tika();
            //var input2 = tika.parseToString(Encoding.ASCII.GetBytes(file));
            var ouput = new StringWriter();

            try
            {
                var metadata = new Metadata();
                var factory = (SAXTransformerFactory)TransformerFactory.newInstance();

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                handler.setResult(new StreamResult(ouput));

                _parser.parse(input, handler, metadata, _context);

                var extractedContent = ouput.toString();

                //if (metadata != null && metadata.get("dc:format") != null &&
                //    metadata.get("dc:format").toLowerCase().Contains(DocumentFormat.pdf.ToString()))
                //{
                //    extractedContent = extractedContent
                //        //.replaceAll("(&#xD;&#xA;|&#xD;|&#xA;)+", "")
                //                                    //.replaceAll("\\t[\\s \u00A0]+", " ")
                //                                    .replaceAll("\\s+\\&\\#xD;\\s+", " ")
                //                                    .replaceAll("\\s+&nbsp;", " ");
                //}

                return extractedContent;
            }
            catch (Exception e)
            {
                throw new Exception("Document conversion was not possible.", e);
            }
            finally
            {
                input.close();
            }
        }

        public string ConvertFileToHtml(byte[] file, string encoding)
        {
            var input = new ByteArrayInputStream(file);
            var ouput = new StringWriter();

            try
            {
                var metadata = new Metadata();
                var factory = (SAXTransformerFactory)TransformerFactory.newInstance();

                var newEncoding = GetPrismEncoding(encoding);
                var encodingDetector = new UniversalEncodingDetector();
                encoding = encodingDetector.detect(input, metadata) != null
                    ? encodingDetector.detect(input, metadata)?.ToString()
                    : newEncoding ?? "UTF-8";

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, encoding);
                handler.setResult(new StreamResult(ouput));

                _parser.parse(input, handler, metadata, _context);

                var extractedContent = ouput.toString();

                var document = new HtmlDocument();
                document.LoadHtml(extractedContent);

                extractedContent = document.DocumentNode.OuterHtml;

                return extractedContent;
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                input.close();
            }
        }

        private static string GetPrismEncoding(string encoding)
        {
            PrismEncoding newEncoding;
            string description = null;
            if (Enum.TryParse(encoding, out newEncoding))
            {
                description = EblaHelpers.GetEnumDescription(newEncoding);
            }
            return description;
        }

        public byte[] ConvertToHtmlWithEmbeddedFiles(byte[] file)
        {
            var input = new ByteArrayInputStream(file);
            var ouput = new StringWriter();

            try
            {
                var metadata = new Metadata();
                var factory = (SAXTransformerFactory)TransformerFactory.newInstance();

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                handler.setResult(new StreamResult(ouput));

                RecursiveParserWrapper wrapper = new RecursiveParserWrapper(_parser, new BasicContentHandlerFactory(
                    BasicContentHandlerFactory.HANDLER_TYPE.TEXT, 60));

                //_parser.parse(input, handler, metadata, _context);
                wrapper.parse(input, handler, metadata, _context);

                var extractedContent = ouput.toString();

                //if (metadata != null && metadata.get("dc:format") != null &&
                //    metadata.get("dc:format").toLowerCase().Contains(DocumentFormat.pdf.ToString()))
                //{
                //    extractedContent = extractedContent
                //        //.replaceAll("(&#xD;&#xA;|&#xD;|&#xA;)+", "")
                //                                    //.replaceAll("\\t[\\s \u00A0]+", " ")
                //                                    .replaceAll("\\s+\\&\\#xD;\\s+", " ")
                //                                    .replaceAll("\\s+&nbsp;", " ");
                //}

                return extractedContent.getBytes("UTF-8");

            }
            catch (Exception e)
            {
                throw new Exception("Document conversion was not possible.", e);
            }
            finally
            {
                input.close();
            }
        }
    }

}

internal class CustomHtmlMapper : DefaultHtmlMapper
{
    public override string mapSafeElement(string name)
    {
        return name.toLowerCase();
    }

    public override string mapSafeAttribute(string elementName, string attributeName)
    {
        return attributeName.toLowerCase();
    }
}
