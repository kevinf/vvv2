﻿using org.apache.tika.extractor;
using System;
using java.io;
using org.xml.sax;
using org.apache.tika.detect;
using org.apache.poi.poifs.filesystem;
using org.apache.tika.io;
using org.apache.tika.metadata;

namespace EblaImpl.Extractor
{
    public class FileEmbeddedDocumentExtractor : EmbeddedDocumentExtractor
    {
        public bool shouldParseEmbedded(Metadata m)
        {
            return true;
        }

        public void parseEmbedded(InputStream inputStream, ContentHandler ch, Metadata m, bool b)
        {
            Detector detector = new DefaultDetector();
            var metadata = new Metadata();
            var name = metadata.get("resourceName");
            org.apache.tika.mime.MediaType contentType = detector.detect(inputStream, metadata);
            if (contentType.GetType().ToString() != "image") return;
            var embeddedFile = name;
            var outputFile = new File(@"C:\toHtml\images\", embeddedFile);
            try
            {
                using (FileOutputStream os = new FileOutputStream(outputFile))
                {
                    var tin = inputStream as TikaInputStream;
                    if (tin != null)
                    {
                        if (tin.getOpenContainer() != null && tin.getOpenContainer() is DirectoryEntry)
                        {
                            POIFSFileSystem fs = new POIFSFileSystem();

                            fs.writeFilesystem(os);
                        }
                        else
                        {
                            IOUtils.copy(inputStream, os);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
