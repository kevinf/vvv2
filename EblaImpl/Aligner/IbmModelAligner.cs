﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using EblaAPI;
using EblaImpl.Translation;

namespace EblaImpl.Aligner
{
    public class IbmModelAligner
    {
        private const float DefaultTranslationTrainingIteration = 4;
        private const int VocabularyNullWid = 0;

        public TranslationData GetTranslationData(TranslationDocument translationDocument,
            List<BestAlignment> bestAlignments)
        {
            var translationUtil = new TranslationUtil();

            SegmentTokens[] baseBestWids, versionBestWids;
            GetBestSegmentWids(translationDocument, bestAlignments, out baseBestWids, out versionBestWids);

            var baseWidList = translationUtil.CreateSegmentWidList(baseBestWids,
                translationDocument.BaseVocabulary, true);

            var versionWidList = translationUtil.CreateSegmentWidList(versionBestWids,
                translationDocument.VersionVocabulary, false);

            if(baseWidList.Count==0 && versionWidList.Count==0) throw new Exception("Base and Version widlists are null.");
            var translationData = ExecuteTranslationTraining(baseWidList,
                versionWidList);

            return translationData;
        }

        private void GetBestSegmentWids(TranslationDocument translationDocument, List<BestAlignment> bestAlignments,
            out SegmentTokens[] baseBestWids, out SegmentTokens[] versionBestWids)
        {
            var baseList = new List<SegmentTokens>();
            var versionList = new List<SegmentTokens>();

            foreach (var alignment in bestAlignments)
            {
                baseList.AddRange(alignment.BaseSegmentList.ToList());
                versionList.AddRange(alignment.VersionSegmentList.ToList());
            }

            baseBestWids = GetBestSegments(translationDocument.BaseDocumentSegmentsWids, baseList);
            versionBestWids = GetBestSegments(translationDocument.VersionDocumentSegmentsWids, versionList);
        }

        private SegmentTokens[] GetBestSegments(SegmentTokens[] segmentWids,
            List<SegmentTokens> segmentsList)
        {
            var bestSegments = new List<SegmentTokens>();
            foreach (var segment in segmentsList)
            {
                var bestSegment = segmentWids.ToList().Find(p => p.Id == segment.Id);
                if (bestSegment != null)
                {
                    bestSegments.Add(bestSegment);
                }
            }
            return bestSegments.ToArray();
        }

        private static TranslationData ExecuteTranslationTraining(List<List<int>> widBaseList,
            List<List<int>> widVersionList)
        {
            var translationData = new TranslationData { BaseTranslationDatas = new List<BaseTranslationData>() };

            for (var iteration = 0; iteration < DefaultTranslationTrainingIteration; iteration++)
            {
                var newTranslationData = ExecuteTranslationTrainingIteration(widBaseList, widVersionList);
                translationData = newTranslationData;
            }

            translationData.Sort();

            return translationData;
        }

        /// <summary>
        /// Executes single translation model training iteration.
        /// </summary>
        /// <param name="baseWidList"></param>
        /// <param name="versionWidList"></param>
        /// <returns></returns>
        private static TranslationData ExecuteTranslationTrainingIteration(List<List<int>> baseWidList,
            List<List<int>> versionWidList)
        {
            var newTranslationData = new TranslationData {BaseTranslationDatas = new List<BaseTranslationData>()};

            if (baseWidList.Count != versionWidList.Count)
                throw new Exception("Base and Version texts must have the same length.");

            for (var iterationCounter = 0; iterationCounter < baseWidList.Count; iterationCounter++)
            {
                foreach (var versionWid in versionWidList[iterationCounter])
                {
                    CreateBaseTranslationDataList(baseWidList, newTranslationData, iterationCounter);

                    var probabilitySum = baseWidList[iterationCounter].Aggregate(0.0d,
                        (current, sourceWid) =>
                            GetInitialTranslationProbability(newTranslationData, versionWid, current, sourceWid));

                    var minProbabilityChange = 1.0 / baseWidList[iterationCounter].Count;

                    foreach (var sourceWid in baseWidList[iterationCounter])
                    {
                        var baseTranslationData = GetBaseTranslationData(newTranslationData, sourceWid);

                        var oldTranslationDataProbability =
                            baseTranslationData.GetInitialTranslationProbability(versionWid);

                        var probabilityChange = oldTranslationDataProbability / probabilitySum;

                        var newBaseData = GetBaseTranslationData(newTranslationData,
                            probabilityChange >= minProbabilityChange ? sourceWid : VocabularyNullWid);

                        var versionTranslationData = newBaseData.GetVersionTranslationData(versionWid);

                        var newTranslationProbability = versionTranslationData?.Probability ?? 0;

                        newBaseData.SetVersionTranslationData(versionWid, probabilityChange + newTranslationProbability);
                    }

                }

            }

            newTranslationData.Normalize();

            return newTranslationData;
        }

        private static void CreateBaseTranslationDataList(List<List<int>> widBaseList,
            TranslationData newTranslationData, int iterationCounter)
        {
            if (newTranslationData.BaseTranslationDatas == null)
            {
                newTranslationData.BaseTranslationDatas = new List<BaseTranslationData>();
            }

            var maxLength = widBaseList[iterationCounter].Max() + 1 >= widBaseList[iterationCounter].Count - 1
               ? widBaseList[iterationCounter].Max() + 1 // Max value inside widlist + 1
               : widBaseList[iterationCounter].Count - 1; // List length without null element

            for (var i = newTranslationData.BaseTranslationDatas.Count; i < maxLength; i++)
            {
                newTranslationData.BaseTranslationDatas.Add(new BaseTranslationData()
                {
                    VersionTranslationDatas = new List<VersionTranslationData>()
                });
            }
        }

        private static TranslationData CreateInitialTranslationData(List<List<int>> widBaseList)
        {
            var newTranslationData = new TranslationData { BaseTranslationDatas = new List<BaseTranslationData>() };

            var results = widBaseList.Select(l => l.Max()).ToList();
            var maxLength = results.Max();
            for (var i = newTranslationData.BaseTranslationDatas.Count; i < maxLength; i++)
            {
                newTranslationData.BaseTranslationDatas.Add(new BaseTranslationData()
                {
                    VersionTranslationDatas = new List<VersionTranslationData>()
                });
            }

            return newTranslationData;
        }

        public static BaseTranslationData GetBaseTranslationData(TranslationData translationData, int wid)
        {
            var newBaseData = new BaseTranslationData();
            if (wid >= translationData.BaseTranslationDatas?.Count) return newBaseData;

            if (translationData.BaseTranslationDatas?[wid] != null)
                newBaseData = translationData.BaseTranslationDatas[wid];

            return newBaseData;
        }

        private static double GetInitialTranslationProbability(TranslationData translationData, int targetWid,
            double probabilitySum, int sourceWid)
        {
            var baseTranslationData = GetBaseTranslationData(translationData, sourceWid);
            var initialProbability = baseTranslationData.GetInitialTranslationProbability(targetWid);

            probabilitySum += initialProbability;
            return probabilitySum;
        }

    }
}
