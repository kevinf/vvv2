﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */

using System.Collections.Generic;
using EblaAPI;
using System.Collections.Specialized;
#if _USE_MYSQL
using EblaDataReader = MySql.Data.MySqlClient.MySqlDataReader;
using EblaConnection = MySql.Data.MySqlClient.MySqlConnection;
using EblaCommand = MySql.Data.MySqlClient.MySqlCommand;
using EblaParameter = MySql.Data.MySqlClient.MySqlParameter;
using EblaTransaction = MySql.Data.MySqlClient.MySqlTransaction;
#else
using System.Data.SQLite;
using EblaDataReader = System.Data.SQLite.SQLiteDataReader;
using EblaConnection = System.Data.SQLite.SQLiteConnection;
using EblaCommand = System.Data.SQLite.SQLiteCommand;
using EblaParameter = System.Data.SQLite.SQLiteParameter;
using EblaTransaction = System.Data.SQLite.SQLiteTransaction;
#endif

namespace EblaImpl
{
    public class SegmentBasedAlignment: OpenableEblaItem
    {
        #region Attributes

        private const string TotalTokensAttributeName = ":totaltokens";

        public int Id { get; set; }
        public int StartPosition { get; set; }
        public int Length { get; set; }
        public int DocumentId { get; set; }
        public int AttributeValue { get; set; }

        public int CorpusId { get; set; }
        public Corpus Corpus { get; set; }

        public int BaseDocumentId { get; set; }
        public Document BaseDocument { get; set; }

        public int VersionDocumentId { get; set; }
        public Document VersionDocument { get; set; }

        public SegmentDefinition[] BaseSegmentDefinitions { get; set; }
        public SegmentDefinition[] VersionSegmentDefinitions { get; set; }

        #endregion

        #region Constructor
        public SegmentBasedAlignment(NameValueCollection configProvider) : base(configProvider)
        {
        }
        #endregion

        public SegmentTokens[] GetSegmentTokens(int documentId, string attributeName)
        {
            var sql = @"SELECT  sd.ID,  sd.StartPosition, sd.Length, sd.DocumentID, sla.Value
                        FROM segmentdefinitions sd
                        INNER JOIN segmentlongattributes sla ON sla.SegmentDefinitionID = sd.ID
                        WHERE sla.Name = @Name
                        AND sd.DocumentID = @DocumentId ";

            var tokens = new List<SegmentTokens>();

            using (var cmd = new EblaCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("Name", attributeName);
                cmd.Parameters.AddWithValue("DocumentId", documentId);

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var idOrdinal = dr.GetOrdinal("ID");
                        var startPositionIdOrdinal = dr.GetOrdinal("StartPosition");
                        var lengthIdOrdinal = dr.GetOrdinal("Length");
                        var docIdOrdinal = dr.GetOrdinal("DocumentID");
                        var valueIdOrdinal = dr.GetOrdinal("Value");
                        var token = new SegmentTokens
                        {
                            Id = dr.GetInt32(idOrdinal),
                            StartPosition = dr.GetInt32(startPositionIdOrdinal),
                            Length = dr.GetInt32(lengthIdOrdinal),
                            DocumentId = dr.GetInt32(docIdOrdinal)
                        };
                        if (attributeName == TotalTokensAttributeName)
                            token.TotalTokensValue = dr.GetInt32(valueIdOrdinal);
                        else
                            token.Attributes = Document.StringToSegmentDefinitionWids(dr.GetString(valueIdOrdinal), token.Id);
                        tokens.Add(token);
                    }
                }
            }

            return tokens.ToArray();
        }

    }
}
