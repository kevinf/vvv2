﻿
namespace EblaImpl.Translation
{
    public class TranslationProbabilities
    {
        /// <summary>
        /// 
        /// </summary>
        public TokensProbability BaseProbability { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public TokensProbability VersionProbability { get; set; }

        /// <summary>
        /// Calculates MeanRatio
        /// </summary>
        public float MeanRatio()
        {
            return VersionProbability.Mean / BaseProbability.Mean;
        }
    }
}
