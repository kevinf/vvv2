﻿namespace EblaImpl.Translation
{
    /// <summary>
    /// Version Translation Data
    /// </summary>
    public class VersionTranslationData
    {
        /// <summary>
        /// Version word id
        /// </summary>
        public int Wid { get; set; }

        /// <summary>
        /// Translation probability
        /// </summary>
        public double Probability { get; set; }

    }
}
