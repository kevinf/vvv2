﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using EblaAPI;

namespace EblaImpl.Translation
{
    public class TranslationUtil
    {
        private const int VocabularyNullWid = 0;

        public List<List<int>> CreateSegmentWidList(SegmentTokens[] segmentList,
            List<AlignmentVocabulary> vocabulary, bool addNullWid)
        {
            var widsList = new List<List<int>>();
            foreach (var segment in segmentList)
            {
                var wids = (from attribute in segment.Attributes
                    select vocabulary.Find(t => t.Token == attribute.Name)
                    into vocabularyToken
                    where vocabularyToken != null
                    select vocabularyToken.Id).ToList();
                if (addNullWid)
                {
                    wids.Add(VocabularyNullWid);
                }
                widsList.Add(wids);
            }
            return widsList;
        }

        public static List<List<int>> AddNullWidToList(List<List<int>> widList)
        {
            var newWidList = new List<List<int>>();
            foreach (var wids in widList)
            {
                if (wids == null) continue;
                wids.Add(VocabularyNullWid);
                newWidList.Add(wids);
            }
            return newWidList;
        }

        public Dictionary<string, int> CreateVocabulary(string[] attributes)
        {
            var vocabulary = new Dictionary<string, int>();
            foreach (var attribute in attributes)
            {
                var tokensLines = attribute.Split(new string[] {Environment.NewLine},
                    StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in tokensLines)
                {
                    var parts = line.Split(new char[] {'\t'}, StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length != 2)
                        throw new Exception("Bad tokens attribute for segment.");

                    var count = 0;
                    if (!int.TryParse(parts[1], out count))
                        throw new Exception("Bad tokens attribute for segment.");

                    if (vocabulary.ContainsKey(parts[0]))
                    {
                        count += vocabulary[parts[0]];
                        vocabulary[parts[0]] = count;
                    }
                    else
                    {
                        vocabulary.Add(parts[0], count);
                    }
                }
            }
            return vocabulary;
        }

        public List<AlignmentVocabulary> CreateAlignmentVocabulary(string[] attributes)
        {
            var vocabulary = new List<AlignmentVocabulary>();
            var vocabularyId = 1;
            foreach (var attribute in attributes)
            {
                var tokensLines = attribute.Split(new string[] {Environment.NewLine},
                    StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in tokensLines)
                {
                    var parts = line.Split(new char[] {'\t'}, StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length != 2)
                        throw new Exception("Bad tokens attribute for segment.");

                    var count = 0;
                    if (!int.TryParse(parts[1], out count))
                        throw new Exception("Bad tokens attribute for segment.");

                    var existingToken = vocabulary.Find(t => t.Token == parts[0]);
                    if (existingToken == null)
                    {
                        var newToken = new AlignmentVocabulary
                        {
                            Id = vocabularyId,
                            Token = parts[0],
                            Count = count
                        };
                        vocabulary.Add(newToken);
                        vocabularyId++;
                    }
                    else
                    {
                        count += existingToken.Count;
                        vocabulary.Find(t => t.Token == parts[0]).Count = count;
                    }
                }
            }
            return vocabulary;
        }

        public List<AlignmentVocabulary> CreateAlignmentVocabulary(SegmentTokens[] segmentList)
        {
            var vocabulary = new List<AlignmentVocabulary>();
            var vocabularyId = 1;

            foreach (var segment in segmentList)
            {
                foreach (var attribute in segment.Attributes)
                {
                    var existingToken = vocabulary.Find(t => t.Token == attribute.Name);
                    if (existingToken == null)
                    {
                        var newToken = new AlignmentVocabulary
                        {
                            Id = vocabularyId,
                            Token = attribute.Name,
                            Count = int.Parse(attribute.Value)
                        };
                        vocabulary.Add(newToken);
                        vocabularyId++;
                    }
                    else
                    {
                        vocabulary.Find(t => t.Token == attribute.Name).Count = existingToken.Count +
                                                                                int.Parse(attribute.Value);
                    }
                }
            }

            return vocabulary;
        }

        public static List<SegmentTokens> GetSubList(SegmentTokens[] list, int fromIndex, int toIndex)
        {
            var subList = new List<SegmentTokens>();
            try
            {
                if (fromIndex >= 0 && toIndex >= 0
                    && fromIndex < list.Length
                    && toIndex <= list.Length)
                {
                    for (var i = fromIndex; i < toIndex; i++)
                    {
                        subList.Add(list[i]);
                    }
                }
            }
            catch (Exception e)
            {
                return subList;
            }
            return subList;
        }

    }
}
