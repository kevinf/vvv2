﻿using System.Collections.Generic;

namespace EblaImpl.Translation
{
    /// <summary>
    /// Translation Data
    /// </summary>
    public class TranslationData
    {
        /// <summary>
        /// List of base translation
        /// </summary>
        public List<BaseTranslationData> BaseTranslationDatas { get; set; }

        /// <summary>
        /// Normalize the list of translations.
        /// </summary>
        public void Normalize()
        {
            if (!(BaseTranslationDatas?.Count > 0)) return;
            foreach (var data in BaseTranslationDatas)
            {
                data.Normalize();
            }
        }

        /// <summary>
        /// Sort the list of translations
        /// </summary>
        public void Sort()
        {
            if (!(BaseTranslationDatas?.Count > 0)) return;
            foreach (var data in BaseTranslationDatas)
            {
                data.Sort();
            }
        }
    }
}
