﻿using System.Collections.Generic;
using System.Linq;

namespace EblaImpl.Translation
{
    /// <summary>
    /// Base Translation Data
    /// </summary>
    public class BaseTranslationData
    {
        /// <summary>
        /// List of version translation
        /// </summary>
        public List<VersionTranslationData> VersionTranslationDatas { get; set; }

        /// <summary>
        /// Initial Translation probability
        /// </summary>
        /// <param name="wid"></param>
        /// <returns></returns>
        public int GetInitialTranslationProbability(int wid)
        {
            return 1;
        }

        /// <summary>
        /// Get version translation data according to the wid.
        /// </summary>
        /// <param name="wid"></param>
        /// <returns></returns>
        public VersionTranslationData GetVersionTranslationData(int wid)
        {
            var newVersionDataValue = VersionTranslationDatas?
                .Find(p => p.Wid == wid);

            var newVersionData = newVersionDataValue ?? new VersionTranslationData();

            return newVersionData;
        }

        /// <summary>
        /// Set values for VersionTranslationData
        /// </summary>
        /// <param name="wid"></param>
        /// <param name="probability"></param>
        public void SetVersionTranslationData(int wid, double probability)
        {
            if (VersionTranslationDatas == null)
            {
                VersionTranslationDatas = new List<VersionTranslationData>();
            }

            var versionData = VersionTranslationDatas.Find(p => p.Wid == wid);
            if (versionData == null)
            {
                versionData = new VersionTranslationData()
                {
                    Wid = wid,
                    Probability = probability
                };
                VersionTranslationDatas.Add(versionData);
            }
            else
            {
                versionData.Probability = probability;
            }
        }

        /// <summary>
        /// Normalize translation data
        /// </summary>
        public void Normalize()
        {
            var totalProbability = 0.0d;
            if (VersionTranslationDatas == null) return;

            totalProbability += VersionTranslationDatas.Sum(data => data.Probability);
            var index = 0;
            foreach (var data in VersionTranslationDatas)
            {
                var newProbability = data.Probability / totalProbability;
                VersionTranslationDatas[index].Wid = data.Wid;
                VersionTranslationDatas[index].Probability = newProbability;

                index++;
            }
        }

        /// <summary>
        /// Sort VersionTranslationDatas based on VersionTranslationDataComparer results.
        /// </summary>
        public void Sort()
        {
            VersionTranslationDatas.Sort(new VersionTranslationDataComparer());
        }

    }
}
