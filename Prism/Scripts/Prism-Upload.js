﻿

var PrismNS = PrismNS || {};

PrismNS.Upload = {};

PrismNS.Upload.instance = null;

$(function () {

    var textareaid = 'doceditor';

    PrismNS.Upload.instance = CKEDITOR.replace(textareaid,
    {
        //toolbar_Bare: [{ name: 'baretb', items: ['Source', '-', 'ToggleMarkers', '-', 'ToggleColour', '-', 'CreateSegment', '-', 'EditSegment', '-', 'DeleteSegment']}],
        toolbar: 'Upload' 
        
    });

    $('#uploadcontent').click(function (e) { PrismNS.Upload.UploadContent(); });
});

PrismNS.Upload.UploadContent = function () {

    var htmlContent = PrismNS.Upload.instance.getData();

    var args = {
        HtmlContent: htmlContent,
        CorpusName: $('#uploadcorpusname').val(),
        NameIfVersion: $('#uploadnameifversion').val()
    }

    var url = _siteUrl + 'Document/UploadContent';

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                alert('Upload succeeded');
                window.location = _siteUrl + 'Corpus?CorpusName=' + args.CorpusName;
                return;

            }
            alert(result.ErrorMsg);
        }
    });
}