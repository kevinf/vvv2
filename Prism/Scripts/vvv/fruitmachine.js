﻿



// init variables 
var translationWidth = 350,  
    sidebarHeight = $(window).height() * 1.5,
    sidebarHeightMarginBottom = 50,
    navHeight = 700,
    navWidth = 85,
    navButtonWidth = 30,
    navTop = 30,
    navLeft = 30,
    navMarginLeft = 50,
    contentMargin = 18,
    connectorWidth = 600, //70
    connectorHeight = $(window).height() - 50,
    sceneLength = 0,
    filterBarWidthMax = 100,
    filterBarWidthMin =70,
    filterWidth = 135,
    margin = 1,
    scale = 0.49,
    lastActiveFilterItem = null,
    layerSpeaker = new Array(),
    layerFilter = new Array(),
    layerCurve = new Array(),
    layerAct = new Array(),
    layerScene = new Array(),
    layerActBarDesc = new Array(),
    layerSceneBarDesc = new Array(),
    translation = new Array(),
    filterStage = new Array(), 
    filterPosY = 0,
    filterPosX = 0;
    connectorStage = new Array(), 
    speakerBar = new Array(),
    speaker = new Array(),
    navStage = new Array(),
    speakerActive = 0,
    actBarMargin = 3,
    sceneBarMargin = 3,
    actSceneBarMargin = 3,
    //actBarWidth = 50,
    actBarHeight = 100,
    actBarWidthSelected = 20,
    //sceneBarWidth = 50,
    navBarWidthMax = 70,
    navBarWidthMin = 10,
    actSceneMargin = 3,
    currentID = 0,
    translationCount = 0,
    contentPositionLeft = new Array(),
    filterStatus = false,
    barPaddingTop = 6,
    barPaddingLeft = 6,
    maxFilterHeight = 0,
    filterScale = 0,
    sidebarTop = 72;


// style var
var filterNormal = '#4c4c4c',
    filterHover  = "#1e1e1e",
    filterHoverHighlighted  = "#5badff",
    filterSelected = '#ffdc61',
    filterHighlighted = '#3399ff',
    actBarFill = "#4c4c4c",
    actBarDescFill = "white",
    actBarFillHover = "#494949",
    actBarDescFillHover = "white",
    actBarFillSelected = "#3a3a3a",
    actBarDescFillSelected = '#3a3a3a',
    filterStrokeWidth = 0,
    filterStrokeWidthColor = ''
    filterStrokeWidthHover = 0.1,
    filterStrokeWidthColorHover = 'black',
    textHighlight = '#ffcc4c';

//typo
var fontNav = "calluna",
    fontFilter = "calluna-sans";

// transitions
var scrollDuration = 100,
    scrollStyle = 'swing';


function initTranslation( url ) {

    var id = currentID;
    var t = $('#'+url+' .tools');
    var txtContainer = $('#'+url+' .text');
    var txt = $('#'+url+' .text .content');

    //hide/show nav button
    //t.append('<button type="button" class="hideNav notVisible" id="hideNav_'+id+'" onClick="showHideNav('+id+');" id="nav_bt_'+id+'"></button>');
    //hide/show filter button
    //t.append('<button type="button" class="hideFilter visible" id="hideFilter_'+id+'" onClick="showHideFilter('+id+');" ></button>');
        
    // create Nav with canvas
    t.append('<div id="nav_canvas_'+id+'" class="nav notVisible"></div>');
    navStage[ id ] = new Kinetic.Stage({
      container: "nav_canvas_" +id,
      width: navWidth,
      height: sidebarHeight
    });
    var nav = $('#nav_canvas_'+id);
    
    // create Nav
    //createNav( id );

    
    // create filter with canvas
    t.append('<div id="filter_canvas_'+id+'" class="filter visible"></div>');
    /*filterStage[ id ] = new Kinetic.Stage({
      container: "filter_canvas_" +id,
      width: filterWidth-20,
      height: sidebarHeight
    });*/
    var filter = $('#filter_canvas_'+id);
    	
    // init translation 
    translation[ id ] = new Translation( id );
	translation[ id ].createFilter( id ); 



	if (id == 0) {
		// create only one connector
		
		// create empty connector canvas  
	    var con = $('.txt_' +id+ ' .connector');
	    connectorStage[ id ] = new Kinetic.Stage({
	        container: "con_" +id,
	        width: connectorWidth + contentMargin ,
	        height: sidebarHeight
	    });
	    // setup connector 
	    layerCurve[ id ] = new Kinetic.Layer();
	    connectorStage[ id ].add(layerCurve[ id ]);
	    con.css({'left': ( translationWidth  + navMarginLeft*3) * id + translationWidth + contentMargin + 67, 'top': 42, 'bottom': 17});

	}
    
    //position translation container
    nav.css({'left': ( translationWidth  + navMarginLeft) * id , 'top': 85, 'height': 0});
    txtContainer.css({'height': $(window).height() - 100 - sidebarHeightMarginBottom });
    $(".tools").css({'height': $(window).height() - 100 - sidebarHeightMarginBottom + sidebarTop });
    $(".filter").children(".kineticjs-content").css({'height': $(window).height() - 150 - 2*sidebarHeightMarginBottom + sidebarTop});
	$('#nav_canvas_' +id).css('left', 500 * id + 35 );
  

    contentPositionLeft[ id ] =  ( translationWidth + connectorWidth + navMarginLeft) * id + navWidth;

	//add scrollbar
 	$("#filter_canvas_" +id + " .kineticjs-content").addClass('nano').wrapInner('<div class="content" />');

    // update connector if init on scroll
    txt.scroll(function() {
       if(filterStatus)  updateConnector( id );
    });

    currentID++; 
    
}


function init(){

    //all Translations
    translationCount = 1;

    // init id, url
    initTranslation('basetextdiv');
    initTranslation('versiondiv');

    $(window).resize(function() {
		var conWidth = $(".txt_1").offset().left - ( $(".txt_0 .text").offset().left + $(".txt_0 .text").outerWidth() + 197 );
		connectorWidth = conWidth;
		if(filterStatus)  updateConnector( 0 );
    });
   
  	//custom scrollbar
    $(".nano").nanoScroller(({ alwaysVisible: true }));
    
    //custom select box style
	$('.customSelect').sb({ animDuration: 10,fixedWidth: true });

 
}

function getRandom(min, max) {
     if(min > max) {
      return -1;
     }
     
     if(min == max) {
      return min;
     }
     
     var r;
     
     do {
      r = Math.random();
     }
     while(r == 1.0);
     
     return min + parseInt(r * (max-min+1));
}

 $(document).ready(function() {
        init();
    });

