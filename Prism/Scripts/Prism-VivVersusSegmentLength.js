﻿

var PrismNS = PrismNS || {};

PrismNS.VivVersusSegmentLengthChart = {};

PrismNS.VivVersusSegmentLengthChart.plot = null;

$(function () {

    EblaNS.FillVariationTypeSelectElm('#variation-type-select');

    var $vs = $('#variation-type-select');

    // switch viv type show on select
    $vs.change(function (event) {

        PrismNS.VivVersusSegmentLengthChart.GetChartData(false);

    });



    PrismNS.VivVersusSegmentLengthChart.GetChartData(true);

    $('#zoomout').click(function (e) { PrismNS.VivVersusSegmentLengthChart.ShowChart(null, null, null, null); });


});

var previousPoint = null;
var previousPointDisplayTime = null;

PrismNS.VivVersusSegmentLengthChart.chartSeries = null;



PrismNS.VivVersusSegmentLengthChart.GetChartData = function (firsttime) {
    var corpusname = $('#corpusname').val();

    var $vs = $('#variation-type-select');

    //alert($vs.val());

    var url = _siteUrl + "Charts/GetVivVersusSegmentLengthData";
    var args = { CorpusName: corpusname, metricTypeVal: parseInt($vs.val()) };


    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {

                PrismNS.VivVersusSegmentLengthChart.PrepareChartData(result);
                
                PrismNS.VivVersusSegmentLengthChart.ShowChart(null, null, null, null);
                return;

            }
            alert('Retrieving chart data failed: ' + result.ErrorMsg);

        }

    });

}



PrismNS.VivVersusSegmentLengthChart.PrepareChartData = function (chartdata) {
    var points = [];

    for (var i = 0; i < chartdata.BaseTextSegmentLengths.length; i++) {
        var segLength = chartdata.BaseTextSegmentLengths[i];
        var vivVals = chartdata.VivValues[i];

        var useAverage = false;
        if (useAverage) {
            var acc = 0.0;
            if (vivVals.length > 0) {
                for (var j = 0; j < vivVals.length; j++) {

                    acc += vivVals[j];
                }

                points.push([segLength, acc / vivVals.length]);
            }


        }
        else {
            for (var j = 0; j < vivVals.length; j++) {
                points.push([segLength, vivVals[j]]);

            }
        }


    }

    PrismNS.VivVersusSegmentLengthChart.chartSeries = { label: 'Unique segment lengths', data: points };

    $('#uniqueseglengthcount').html(chartdata.BaseTextSegmentLengths.length + " unique segment lengths, " + points.length + " data points." );
}


PrismNS.VivVersusSegmentLengthChart.ShowChart = function (xaxismin, xaxismax, yaxismin, yaxismax) {


    var seriesList = new Array();

    seriesList.push(PrismNS.VivVersusSegmentLengthChart.chartSeries);


    var options = {
        series: { lines: { show: false }, points: { show: true} },
        xaxes: [{ position: 'top'}],

        grid: { hoverable: true} //,
        
            ,
        selection: { mode: "xy" }

    };

    if (xaxismin != null) {
        options.xaxis = { min: xaxismin, max: xaxismax };
        options.yaxis = { min: yaxismin, max: yaxismax };
    }

    PrismNS.Utils.EnableButton('zoomout', xaxismin != null);

    PrismNS.VivVersusSegmentLengthChart.plot = $.plot($('#chart'), seriesList, options);

    var ctx = PrismNS.VivVersusSegmentLengthChart.plot.getCanvas().getContext("2d");
    ctx.textAlign = 'center';

    $("#chart").bind("plotselected", function (event, ranges) {

        PrismNS.VivVersusSegmentLengthChart.ShowChart(ranges.xaxis.from, ranges.xaxis.to, ranges.yaxis.from, ranges.yaxis.to);

    });

}


function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 5,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#fee',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
}
