﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<EblaAPI.UserInfo[]>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	VVV - Admin</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container">
	<div class="page-header">
		<h1>Users</h1>
		<p><%: Html.ValidationSummary(false) %></p>            
	</div>

    <div class="row">
    	<div class="span12">
    		<table class="table table-bordered">
		        <thead>
		        	<tr>
		        		<th>Username</th>
		        		<th>Administrator</th>
		        		<th></th>
		        		<th></th>
		        	</tr>
		        </thead>

		        <tbody>
		        	<% foreach (EblaAPI.UserInfo ui in Model) { %>
		        	<tr>
		        	<td><%: ui.Username  %></td>
		        	<td><%: (ui.IsAdmin ? "Yes" : "No") %></td>
		        	<td><%: Html.ActionLink("Edit", "UserAdmin", new { Username = ui.Username }, new { @class = "btn" })%></td>
		        	<td>
						<% if (string.Compare(ui.Username, Prism.Controllers.AccountController.SessionEblaUsername) != 0)
		           		   {  %>
				    	<a href="<%: Url.Action( "DeleteUser", new { Username = ui.Username } ) %>"
	                        class="btn btn-mini btn-danger" onclick="return confirm('Are you sure you wish to delete this user?');">
	                        <i class="icon-trash icon-white"></i>
	                    </a>
		        		<% } %>
		        	</td>
		        	</tr>
		        	<% } %>
		        </tbody>
			</table>
    	</div>
    </div>
    <div class="row">
    	<div class="span12">
    		<%: Html.ActionLink("Create New", "CreateUser", null, new { @class = "btn" })%>
    	</div>
    </div>
    <div id="push"></div>
</div>


    

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
