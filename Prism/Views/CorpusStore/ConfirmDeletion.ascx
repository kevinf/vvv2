﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%--<link href="<%= Url.Content("~/Content/bootstrap/css/bootstrap.css") %>" rel="stylesheet" type="text/css" />--%>

<%--<div class="modal fade" id="deleteConfirmModal" tabindex="-1" role="dialog" 
        aria-labelledby="deleteLabel" aria-hidden="true">
    <div class="modal-header">
        <h4 class="title" id="deleteLabel">Are you sure you want to delete?</h4>
    </div>
    <div class="modal-body" id="notificationPreview">
        <p>Loading content...</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" id="deleteConfirm">Delete</button>
    </div>
</div>--%>

<div class="modal fade" id="ConfirmDeletionModal" tabindex="-1" role="dialog" aria-labelledby="deleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="deleteLabel">Deleting a corpus</h4>
            </div>
            <div class="modal-body">
                <p>You have selected to delete this corpus.</p>
                <p>
                    If this was the action that you wanted to do,
                    please confirm your choice, or cancel and return
                    to the page.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="deleteConfirm">Delete</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

