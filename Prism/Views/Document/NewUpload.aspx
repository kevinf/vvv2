﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.UploadFilesResult>" %>

<%@ Import Namespace="Prism.HtmlHelpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - upload multiple documents
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">  
    <%--<link href="<%= Url.Content("~/Content/newer/css/bootstrap.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/newer/css/bootstrap-theme.min.css") %>" rel="stylesheet" type="text/css" />--%>
    <link href="<%= Url.Content("~/Content/bootstrap/css/bootstrap.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/bootstrap/css/bootstrap-theme.min.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/newer/css/jquery.fileupload.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%= Url.Content("~/Scripts/newer/jquery-1.9.1.min.js") %>" type="text/javascript"></script>
    <script type="text/javascript">
        var $$ = jQuery;
    </script>
    <script src="<%= Url.Content("~/Scripts/newer/jquery.ui.widget.js") %>" type="text/javascript"></script>
    <%--<script src="<%= Url.Content("~/Scripts/newer/bootstrap.min.js") %>" type="text/javascript"></script>--%>
    <script src="<%= Url.Content("~/Scripts/bootstrap.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/newer/jquery.fileupload.js") %>" type="text/javascript"></script>
    
    <script type="text/javascript">
        
        jQuery.noConflict();

        $$(document).ready(function () {
            $$('#fileupload').fileupload({
                dataType: 'json',
                url: '/Document/UploadFiles',
                autoUpload: true,
                done: function (e, data) {
                    $$('.file_name').html(data.result.name);
                    $$('.file_type').html(data.result.type);
                    $$('.file_size').html(data.result.size);
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $$('.progress .progress-bar').css('width', progress + '%');
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<div class="container">
        <div class="page-header">
            <h2>
                Upload
                <%: Model.CorpusName %>:
                <% if (string.IsNullOrEmpty(Model.NameIfVersion))
                   { %>
                base text
                <% }
                   else
                   { %>
                version '<%:Model.NameIfVersion %>'<% } %>
            </h2>
            <p>
                NOTE: Uploading a document will delete any existing segmentation and alignment information
                relating to the document.</p>           
        </div>
        <%: Html.ValidationSummary(true) %>
        <% using (Html.BeginForm(null, null, FormMethod.Post, new { enctype = "multipart/form-data" }))
           {%>
        <%: Html.HiddenFor(model => model.CorpusName, new { id = "uploadcorpusname" })%>
        <%: Html.HiddenFor(model => model.NameIfVersion, new { id = "uploadnameifversion" })%>
        <%--<fieldset>
            <legend>Upload file</legend>
            <label for="file">
                Filename:</label>
            <input type="file" name="file" id="file" />
            <%: Html.EnumDropDownListFor(model => model.encoding) %>
            <p> <input type="file" name="file" id="btnFileUpload" class="btn-primary" value="Upload file" /> </p>
            <div id="progressbar" style="width:100px;display:none;">
                <div>
                    saadsd
                </div>
            </div>
        </fieldset>--%>
        
        <div class="container">
		    <span class="btn btn-success fileinput-button">
			    <i class="glyphicon glyphicon-plus"></i>
			    <span>Add files...</span>
		        <input id="fileupload" type="file" name="files[]" multiple/>
		    </span>
		    <br />
		    <div class="progress">
			    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
				    <span class="sr-only">0% complete</span>
			    </div>
		    </div>
		    <br />
		    <div class="file_name"></div>
		    <br />
		    <div class="file_type"></div>
		    <br />
		    <div class="file_size"></div>
	    </div>
        <%--<% } %>
        
        
        <div>
            <%: Html.ActionLink("Back to Corpus", "Index", "Corpus", new { CorpusName = Model.CorpusName }, new { @class="btn" })%>
        </div>
    </div>--%>
</asp:Content>
