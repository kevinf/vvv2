﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Prism.Models.SegmentDefinitionModel>" %>
<%@ Import Namespace="Prism.HtmlHelpers" %>
<%: Html.ValidationSummary(false) %>
<span class="display-label">Segment ID: </span><span class="display-field">
    <%: Model.ID.ToString() %></span>
<span class="display-label">Segment start: </span><span class="display-field">
    <%: Model.startPos.ToString() %></span>
<span class="display-label">Segment length: </span><span class="display-field">
    <%: Model.length.ToString() %></span><br />
<span class="display-label">Segment content: </span><span class="display-field">
    <%: Model.Content  %></span>

    <%: Html.HiddenFor(x => x.startPos) %>
    <%: Html.HiddenFor(x => x.length) %>
    <%: Html.HiddenFor(x => x.CorpusName) %>
    <%: Html.HiddenFor(x => x.NameIfVersion) %>
    <%: Html.HiddenFor(x => x.ID) %>
<fieldset>
    Content behaviour: <%: Html.EnumDropDownListFor(model => model.ContentBehaviour) %>
    <legend>Attributes</legend>
    <table id="attribtable">
        <thead>
            <tr>
                <th>
                    <span class="editor-label">Name</span>
                </th>
                <th>
                    <span class="editor-label">Value</span>
                </th>
                <th>
                </th>
            </tr>
        </thead>
        <tbody>
            <% for (int i = 0; i < Model.Attributes.Length; i++)
               {
                   //EblaAPI.PredefinedSegmentAttribute pre = null;
                   string indexval = i.ToString();
                   string rowid = "attribrow-" + indexval;
                   //string nameselectname = "Attributes[" + indexval + "].Name";
                   //string valueinputname = "Attributes[" + indexval + "].Value";
                   string rowhtml = Prism.Controllers.CorpusController.AttributeRowHtml("Attributes", Model.CorpusName, i, Model.Attributes[i].Name, Model.Attributes[i].Value, Model.ID == -1);
            %>
            <tr id='<%=rowid%>'>
                <%=rowhtml %>
            </tr>
<%--            <tr id='<%=rowid%>'>
                <td>
                    <input type='hidden' name='Attributes.Index' value='<%=indexval %>' />
                    <span class='editor-field'>
                        
                        <select name="<%=nameselectname %>">
                            <% foreach (var p in Model.PredefinedSegmentAttributes)
                               {
                                   string selected = string.Empty;
                                   if (string.Compare(p.Name, Model.Attributes[i].Name) == 0)
                                   {
                                       selected = "selected";
                                       if (p.AttributeType == EblaAPI.PredefinedSegmentAttributeType.List)
                                            pre = p;
                                   }
                            %>
                            <option <%=selected %> value="<%=p.Name %>">
                                <%=p.Name %></option>
                            <% } %>
                        </select>
                    </span>
                </td>
                <td>
                    <span class='editor-field'>
                    <% if (pre != null)
                       { %>
                        <select name="<%=valueinputname %>" >
                            <% foreach (string s in pre.Values)
                               {
                                   string selected = string.Empty;
                                   if (string.Compare(s, Model.Attributes[i].Value) == 0)
                                       selected = "selected";
                                                                      
                                   %>
                               <option <%=selected %>  value="<%=s %>" ><%=s %></option>

                            <% } %>
                        </select>
                    <% }
                       else
                       { %>
                        <input type='text' class='half' name='<%=valueinputname %>' value='<%=Model.Attributes[i].Value %>' />

                        <% } %>
                    </span>
                </td>
                <td>
                    <input type='button' onclick='deleteattrib(this)' value='Delete' />
                </td>
            </tr>
--%>            <% } %>
            <%--<%= Html.SegAttribRows( Model) %>--%>
        </tbody>
    </table>
    <input type="button" value="Add" id="addattrib" />
</fieldset>
<input type="hidden" id="nextattribrowindex" value="<%= ViewData["nextattribrowindex"] %>" />
<%--<select id="predefinednames" style="display: none" >
<% foreach (var p in Model.PredefinedSegmentAttributes)
    {
%>
<option value="<%=p.Name %>">
    <%=p.Name %></option>
<% } %>
</select>--%>
<script type="text/javascript">
    $(function () {

        $('#addattrib').click(function (e) { AddAttribRow('nextattribrowindex', 'Attributes', 'attribtable', 'attribrow-'); });
    });


</script>
