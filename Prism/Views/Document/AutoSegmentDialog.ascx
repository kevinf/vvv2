﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Prism.Models.AutoSegmentationModel>" %>
<div>
    <%: Html.HiddenFor(x => x.CorpusName) %>
    <%: Html.HiddenFor(x => x.VersionName) %>
    <br />
    Auto-segmentation options within VVV are currently quite crude - segmenting around a tag pair, after a given tag, or on punctuation. You may wish to segment the file
    prior to uploading, using more sophisticated scripts. For information on how to include segmentation within the uploaded file, contact the VVV team, or export 
    a segmented file from VVV for an example.<br /><br />
    <asp:Panel   ID="Panel1" runat="server" GroupingText="Tag-based">
    <input name="Option" id="Radio1" type="radio" value="around" checked="checked" /> Segment around tag pair<br />
    <input name="Option" id="Radio2" type="radio" value="after" />Segment after tag<br />

        Tag: 
    <input id="Tag" name="Tag" type="text" value="p" /><br />
    </asp:Panel>
    <br />
    <input name="Option" id="Option" type="radio" value="" disabled="disabled" />Segment on punctuation<br />



</div>