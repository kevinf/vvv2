﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	VVV - Error
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container">
		<div class="page-header">
			<div class="alert alert-error">
    		<strong>Error:</strong> <%: ViewData["message"] %>
			</div>
		</div>
		<div id="push"></div>
	</div>
</asp:Content>
