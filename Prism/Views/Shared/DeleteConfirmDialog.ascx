﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<link href="<%= Url.Content("~/Content/bootstrap/css/bootstrap.css") %>" rel="stylesheet" type="text/css" />

<div class="modal fade" id="deleteConfirmModal" tabindex="-1" role="dialog" 
        aria-labelledby="deleteLabel" aria-hidden="true">
    <div class="modal-header">
        <h4 class="title" id="deleteLabel">Are you sure you want to delete?</h4>
    </div>
    <div class="modal-body" id="notificationPreview">
        <p>Loading content...</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" id="deleteConfirm">Delete</button>
    </div>
</div>

<%--<% using (Html.BeginForm( null, null, FormMethod.Post, new { id="docextractform", @class="form-horizontal" }))
					{ %>
					<%: Html.ValidationSummary(false) %>
					<div class="pull-right">
						<input type="button" value="View" id="viewbutton" />
					</div>--%>


