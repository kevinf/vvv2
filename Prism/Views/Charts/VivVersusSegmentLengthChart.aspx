﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.BaseCorpusModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Corpus:
    <%: Model.CorpusName%>
    - Viv versus segment length
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

       <div class="container">
        <div class="page-header">
            <h1>
                <%: Model.CorpusName%>
                - Viv versus segment length</h1>
        </div>
         <div class="row">
        <div class="control-group">
            
            </div>
            <div class="sorting container">
                <div class="control-label">
                    <a href="<%= Url.Content("~/Home/Project#eddyviv") %>" class="tt pull-left" title="The maths for <i>Eddy</i> and <i>Viv</i> algorithms is experimental. Click for more info"
                        rel="tooltip" data-placement="right"><i class="icon-question-sign pull-right"></i>
                        Variation metric:</a>
                </div>
                <div class="controls">
                    <select id="variation-type-select">
                        <!-- to be filled by JS -->
                    </select>
                </div>
            </div>

             <input type="button" id="zoomout" value="Zoom out" disabled="disabled" />
            <div id="chartxaxislabel">
<%--                <div class="pull-right subnav-item-right">
                <input type="button" id="zoomout" value="Zoom out" disabled="disabled" />
                </div>--%>
                Segment length in words
                </div>
            <div id="chart">
            </div>
            <br />
            <span id="uniqueseglengthcount"></span>
            <div style="height:200px"></div>

        </div>
    </div>
    <input id="corpusname" type="hidden" value="<%: Model.CorpusName %>" />
    

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
 <script src="<%= Url.Content("~/Scripts/Prism-VivVersusSegmentLength.js") %>" type="text/javascript"></script>
  <style type="text/css">
        #legendcontainer
        {
            width: 1200px;
            height: 225px;
        }
        
        #chart
        {
            width: 1200px;
            height: 600px;
        }
        
        #chartxaxislabel
        {
            width: 1200px;
            text-align: center;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HelpMenuItem" runat="server">
</asp:Content>
