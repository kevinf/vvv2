﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EblaAPI.SegmentVariation>" %>

<div id="vivtestdiv">


<span class="display-label">Base text segment ID: </span><span class="display-field">
    <%: Model.BaseTextSegmentID.ToString() %></span>
<span class="display-label">Viv value: </span><span class="display-field">
    <%: Model.VivValue.ToString() %></span><br />

<span class="display-label">Segment content: </span><span class="display-field">
    <%: Model.BaseTextContent  %></span>
<br />
<br />
-- Eddy values (click column headers to sort)
<table id="vivtesttable" class="sortable" >
<thead>
<tr>
<th >
Version
</th>
<th>
Eddy value
</th>
<th>
Content
</th>
</tr>
</thead>
<tbody>
<%
    for (int i = 0; i < Model.VersionSegmentVariations.Length; i++)
    {
     %>
<tr >
<td>
<%: Model.VersionSegmentVariations[i].VersionName %>
</td>
<td>
<%: Model.VersionSegmentVariations[i].EddyValue.ToString() %>
</td>
<td>
<%: Model.VersionSegmentVariations[i].VersionText %>
</td>

</tr>

     <%
    }
          %>
</tbody>
</table>

<br />
-- Tokens (click column headers to sort)
<table class="sortable" id="vivtesttable2">
<thead>
<tr >
<th>Token</th>
<th>Average count</th>
</tr>
</thead>
<tbody>
<% for (int i = 0; i < Model.TokenInfo.Length; i++)
   { %>

   <tr >
   <td><%: Model.TokenInfo[i].Token %> </td>
   <td><%: Model.TokenInfo[i].AverageCount.ToString() %> </td>
   </tr>

   <% 
   }
          
        %>
</tbody>
</table>
</div>
<script type="text/javascript"  >
    //alert('aha');

    $(function () {

        //alert('aha2');
        sorttable.forceinit($('#vivtesttable')[0]);
        sorttable.forceinit($('#vivtesttable2')[0]);
        //alert('aha3');

    });


</script>
