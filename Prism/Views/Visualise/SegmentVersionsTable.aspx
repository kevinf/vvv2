﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.SegmentVersionsTableModel>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
        <script src="<%= Url.Content("~/Scripts/vvv/d3/d3.v2.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/chroma/chroma.min.js") %>" type="text/javascript"></script>

    <script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-SegmentVersionsTable.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-FindSegmentDialog.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/jquery-ui-1.10.3.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/jquery.jqGrid.min.js") %>" type="text/javascript"></script>


    <link href="<%= Url.Content("~/Content/css/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/css/ui-lightness/jquery-ui-1.10.3.custom.min.css") %>"
        rel="stylesheet" type="text/css" />
    <style type="text/css">
        /*.celltop 
        {
            position:absolute;
            top: 0px;
        }*/
        .cellbottom
        {
            position: absolute;
            bottom: 0px;
        }
        
        /* make jqgrid do word wrap on cells with long text */
        .ui-jqgrid tr.jqgrow td
        {
            word-wrap: break-word; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap; /* CSS3 */
            white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
            white-space: -pre-wrap; /* Opera 4-6 */
            white-space: -o-pre-wrap; /* Opera 7 */
            overflow: hidden;
            height: auto;
            vertical-align: top;
            padding-top: 3px;
            padding-bottom: 3px;
            position: relative;
        }
        
        /* make jqgrid do word wrap on headers with long text */
        th.ui-th-column div
        {
            white-space: normal !important;
            height: auto !important;
            padding: 2px;
        }
        
        .svdchart
        {
            width: 70px;
            height: 60px;
        }
        
        #modalContent input[type=text].segmentsearchfloat
        {
            width: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="height: 70px">
    </div>
    <div>
        <%: Html.HiddenFor(model => model.CorpusName, new { id = "corpusname" })%>
        <a href="<%= Url.Content("~/Home/Project#eddyviv") %>" class="tt" title="The maths for <i>Eddy</i> and <i>Viv</i> algorithms is experimental. Click for more info"
            rel="tooltip" data-placement="right">Variation metric:<i class="icon-question-sign"></i></a>
        <select id="variation-type-select">
            <!-- to be filled by JS -->
        </select>
        Attribute:
        <select id="attribname">
            <% foreach (var a in Model.AttribNames.Keys)
               { %>
            <option value="<%: Model.AttribNames[a] %>">
                <%: a %></option>
            <% } %>
        </select>
        <input type="checkbox" id="showsvd" />Show SVD charts
        <br />
        <input type="button" value="Add" id="addrows" title="Search for segments to add to the grid" />
        <input type="button" value="Remove" id="deleterows" title="Remove checked rows from the grid" />
        <%--<input type="button" value="Export" id="exportall" title="Export all rows to CSV" />--%>
        <%-- <%: Html.ActionLink("Export", "ExportVersionsTable", "Visualise", new { CorpusName = Model.CorpusName, AttribName="nnn"  }, new { @class = "btn btn-mini" })%>--%>
        <div style="float: right">
            <% using (Html.BeginForm("ExportVersionsTable", "Visualise"))
               { %>
            <input type="hidden" name="CorpusName" id="submitcorpusname" />
            <input type="hidden" name="AttribName" id="submitattribname" />
            <input type="hidden" name="metricTypeVal" id="submitmetrictypeval" />
            <input type="submit" value="Export" />
            <% } %>
        </div>
        <input type="checkbox" checked="checked" id="sizetowindow" />Fit grid to window
    </div>
    <table id="list2">
    </table>
    <div id="pager2">
    </div>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="HelpMenuItem" runat="server">
</asp:Content>--%>
