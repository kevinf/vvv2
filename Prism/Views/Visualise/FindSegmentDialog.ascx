﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Prism.Models.FindSegmentModel>" %>
<div  style="height: 500px" >

Find segments containing:
<input type="text" id="segmentsearchtext" />
<input type="button" id="findsegment" value="Find" /><br />
<input class="texttosearchradio" type="radio" name="texttosearch" id="segmentsearchbasetext"
    checked="checked" value="basetext" />In base text &nbsp;
<input class="texttosearchradio" type="radio" name="texttosearch" id="segmentsearchversion"
    value="version" />In version:
<%: Html.DropDownListFor(model => model.VersionToSearch, (System.Collections.Generic.IEnumerable<SelectListItem>)ViewData["versionlist"], new { id = "versiontosearch" }) %>
<br />
Viv between
<input type="text" class="segmentsearchfloat" id="segmentsearchvivfloor" />
and
<input type="text" class="segmentsearchfloat" id="segmentsearchvivceiling" />, Eddy
between
<input type="text" class="segmentsearchfloat" id="segmentsearcheddyfloor" />
and
<input type="text" class="segmentsearchfloat" id="segmentsearcheddyceiling" />
<br />
<div style="width: 100%; border: 1px; border-color: Black; border-collapse:separate; border-style:solid">
Segment filter:
<table id="attribfiltertable" class="table">
    <thead>
        <tr>
            <th>
                Name
            </th>
            <th>
                Value
            </th>
            <th>
            </th>
        </tr>
    </thead>
</table>
<input type="button" value="Add" id="addfilterattrib" />
</div>
Within: 
<%: Html.DropDownListFor(model => model.WithinSegmentID, (System.Collections.Generic.IEnumerable<SelectListItem>)ViewData["TOCSegments"]) %>

<input type="hidden" id="nextattribfilterrowindex" value="1" />
<%--<input type="checkbox" id="findsegcheckall" />--%>
<div id="findsegmentresults">
</div>
</div>
<script type="text/javascript">
    $(function () {

        $('#addfilterattrib').click(function (e) { PrismNS.FindSegmentDialog.AddFilterAttrib();  });


//        $('#findsegcheckall').click(function (e) { PrismNS.FindSegmentDialog.CheckAll();  });
  

        $('#findsegment').click(function (e) {PrismNS.FindSegmentDialog.Find();  });

    });


</script>
