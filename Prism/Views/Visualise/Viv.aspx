<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV -
    <%: ViewData["corpusname"] %>
    - Viv visualisation
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="subnav subnav-fixed">
        <div class="sorting container">
            <div class="pull-right subnav-item-right">
                <div id="sort-direction" class="option-set" data-option-key="sortAscending">
                    <div class="btn-group pull-right">
	                	<a href="#sortAscending=true" data-option-value="true" class="btn btn-mini optionlink disabled">Asc</a>
	                    <a href="#sortAscending=false" data-option-value="false" class="btn btn-mini optionlink">Desc</a>
	                </div>
	                <div class="headline pull-right">ORDER</div>
                </div>
            </div>
            <div class="pull-right subnav-item-right">
                <div id="sort-by" class="option-set" data-option-key="sortBy">
                  <form class="form-horizontal viv-sort">
						<div class="control-label">
						<a href="#" class="tt pull-left" title="Select <i>Eddy</i> to sort translations in order of how much each one differs from all the others." rel="tooltip" data-placement="right"><i class="icon-question-sign pull-right"></i>SORT BY</a>
						</div>
						<div class="controls">
							<select id="viv-sort-select" data-option-key="sortBy">
								<option data-option-value="eddy">Eddy value</option>
								<option data-option-value="name">Version Name</option>
								<option data-option-value="length">Segment length</option>
								<option data-option-value="year">Reference Date</option>
							</select>
						</div>

					</form>
                </div>
            </div>
            <div>
                <form class="form-horizontal viv-select">
					<div class="control-group">
						<div class="control-label">
						<a href="<%= Url.Content("~/Home/Project#eddyviv") %>" class="tt pull-left" title="The maths for <i>Eddy</i> and <i>Viv</i> algorithms is experimental. Click for more info" rel="tooltip" data-placement="right"><i class="icon-question-sign pull-right"></i>VIV TYPE</a>
						</div>

						<div class="controls">
							<select id="viv-type-select">
								<!-- to be filled by JS -->
							</select>
						</div>	
					</div>
				</form>
            </div>
            <div class="row">
	            <div id="viv-adjust" class="span12">
	                <table>
	                    <tr>
	                        <td>
	                            Viv floor:
	                        </td>
	                        <td id="floorslider">
	                        </td>
	                        <td id="floorsliderval" style="padding-left:30px"></td>
	                    </tr>
	                    <tr>
	                        <td>
	                            Viv ceiling:
	                        </td>
	                        <td id="ceilingslider">
	                        </td>
	                        <td id="ceilingsliderval" style="padding-left:30px"></td>
	                    </tr>
	                    <tr>
	                    <td>
	                    <input type="button" id="applyvivlimits" value="Apply" />
	                    <a href="#" class="tt" title="Focus on specific Viv value ranges by raising the <i>floor</i> or lowering the <i>ceiling</i> of the values displayed." rel="tooltip" data-placement="right"><i class="icon-question-sign"></i></a>
	                    </td>
	                    </tr>
	                </table>
	            </div>
            </div>
        </div>
    </div>
    <div class="container viv withSubnav">
        <div id="panel" class="row">
        	<div class="scroll-wrapper span6">
	            <div id="basetextdiv" class="version scroll-content">
	                <div class="title">
	                    <h1>
	                        <%: ViewData["corpusname"] %><br />
	                        <small>
	                            <%: ViewData["corpusdescription"] %></small></h1>
	                </div>
	                <div class="text">
	                    <% Response.Write(ViewData["basetextcontent"].ToString()); %>
	                </div>
	            </div>
	        </div>
            <div class="scroll-wrapper span6">
            	<div id="eddystats" class="scroll-content">
            		<p class="muted">Click on a segment to the left to get Eddy results for that segment.</p>
            	</div>
            </div>
        </div>
        
        <input type="hidden" id="corpusname" value="<%=ViewData["corpusname"] %>" />
        <input type="hidden" id="vivfloor" value="<%=ViewData["vivfloor"] %>" />
        <input type="hidden" id="vivceiling" value="<%=ViewData["vivceiling"] %>" />
        <input type="hidden" id="selectedsegid" value="-1" />
        <input type="hidden" id="basetextlangcode" value="<%=ViewData["basetextlangcode"] %>" />
        <input type="hidden" id="versionlangcode" value="<%=ViewData["versionlangcode"] %>" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-Formatting.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/d3/d3.v2.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/chroma/chroma.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/viv/viv.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/jquery.isotope.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/nanoScroller/jquery.nanoscroller.js") %>" type="text/javascript"></script>
    <link href="<%= Url.Content("~/Content/css/nanoscroller.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">        window.dhx_globalImgPath = "../Images/dhx/";</script>
    <script src="<%= Url.Content("~/Scripts/dhx/dhtmlxcommon.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/dhx/dhtmlxslider.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/dhx/ext/dhtmlxslider_start.js") %>" type="text/javascript"></script>
    <link href="<%= Url.Content("~/Content/dhx/dhtmlxslider.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%= Url.Content("http://use.typekit.com/oiq3xqg.js") %>" type="text/javascript"></script>
    
    <script type="text/javascript">        try { Typekit.load(); } catch (e) { }</script>
        <script type="text/javascript" src="https://apis.google.com/js/client.js?onload=load"></script>
</asp:Content>

<asp:Content ID="Help" ContentPlaceHolderID="HelpMenuItem" runat="server">
<a id="help-popover" href="#"><i class="icon-question-sign icon-white"></i> What&#8217;s this?</a>
<div id="help-content" style="display:none">
	<p>
	<i>Eddy</i> and <i>Viv</i> are experimental algorithms, working with concordance data, to help us explore multiple translations.
	The varying colour under the Base Text represents <i>Viv</i> values. <i>Viv</i> is a measure of how much variation there is among
	translations of a segment. Darker colour means greater variation. The <i>Viv</i> value of a Base Text segment derives from the
	<i>Eddy</i> values for all the translations of it. <i>Eddy</i> is a measure of how much a translation of a segment differs from all others.<br />
	<a href="<%= Url.Content("~/Home/Project/#eddyviv") %>">More about Eddy and Viv</a>
	</p>
	
</div>
</asp:Content>