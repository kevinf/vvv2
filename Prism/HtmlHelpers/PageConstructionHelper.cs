﻿using System;
using System.Web.Mvc;

namespace Prism.HtmlHelpers
{
    public static class PageConstructionHelper
    {
        public static IDisposable BeginPrismPage(this
            HtmlHelper helper, bool fluid = false)
        {
            var tag = fluid
                ? "<div class=\"container-fluid\">"
                : "<div class=\"container\">";
            helper.ViewContext.Writer.Write(tag);
            return new PrismPage(helper);
        }
    }
}