﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EblaImpl;
using System.Configuration;
using System.Linq;
using EblaImpl.Aligner;
using EblaImpl.Calculator;
using EblaImpl.Translation;

namespace Ebla.Tests.Integration
{
    [TestClass]
    public class TranslationAlignerTests
    {
        private const string TotalTokensAttributeName = ":totaltokens";
        private const string TokensAttributeName = ":tokens";
        private const string WidsAttributeName = ":wids";

        [TestMethod]
        public void ShouldApplyMooreAlignment()
        {
            var alignment = new TranslationAligner(ConfigurationManager.AppSettings);
            var bestAlignments = alignment.ApplyMooreAlignment(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName(),
                Helpers.GetTestUsername(), Helpers.GetTestPassword());
            Assert.IsNotNull(bestAlignments);
        }

        [TestMethod]
        public void ShouldGetSegmentTokens()
        {
            var segment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var result = segment.GetSegmentTokens(34, TokensAttributeName);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldGetSegmentTokensWidsAttributeName()
        {
            var segment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var result = segment.GetSegmentTokens(214, WidsAttributeName);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCalculateTokensProbabilities()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(214, TotalTokensAttributeName);

            var segmentCalculator = new SegmentCalculator();
            var result = segmentCalculator.CalculateProbabilities(segmentDefinitions, null);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCreateAlignmentVocabulary()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(214, TokensAttributeName);

            var translationUtil = new TranslationUtil();
            var result = translationUtil.CreateAlignmentVocabulary(segmentDefinitions.ToArray());
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCalculateWordTranslationProbabilities()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(214, TokensAttributeName);
            var wids = sentenceAlignment.GetSegmentTokens(214, WidsAttributeName);

            var translationUtil = new TranslationUtil();
            var alignmentVocabulary = translationUtil.CreateAlignmentVocabulary(segmentDefinitions.ToArray());

            var wordCalculator = new WordCalculator();
            var result = wordCalculator.CalculateProbabilities(wids, alignmentVocabulary);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCreateSegmentWidList()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(34, TokensAttributeName);
            var segmentWids = sentenceAlignment.GetSegmentTokens(34, WidsAttributeName);

            var translationUtil = new TranslationUtil();
            var alignmentVocabulary = translationUtil.CreateAlignmentVocabulary(segmentDefinitions.ToArray());

            var result = translationUtil.CreateSegmentWidList(segmentWids, alignmentVocabulary, true);

            Assert.IsNotNull(result);
        }

    }
}
