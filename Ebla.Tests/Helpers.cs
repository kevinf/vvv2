﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;
using EblaImpl;
using System.Configuration;
using System.IO;
using EblaImpl.Extractor;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ebla.Tests
{
    internal class Helpers
    {
        public static string GetTestUsername()
        {
            return "admin";
        }

        public static string GetTestPassword()
        {
            return "VVVAdmin1";
        }

        public static string GetTestCorpusName()
        {
            return "TestCorpus";
        }

        public static string GetTestCorpusName1()
        {
            return "TestCorpus1";
        }

        public static string GetTestCorpusName2()
        {
            return "TestCorpus2";
            //"Kevin Tags";
        }

        public static string GetVersionTextName()
        {
            return "Test Portuguese";
        }

        internal static ICorpus GetCorpus(string name)
        {
            
            ICorpus corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);

            if (!corpus.Open(name, Helpers.GetTestUsername(), Helpers.GetTestPassword()))
                throw new Exception("You do not have permission to access the corpus.");

            return corpus;
        }

        public static ICorpusStore GetCorpusStore()
        {
            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            store.Open(Helpers.GetTestUsername(), Helpers.GetTestPassword());

            return store;
        }

        internal static IDocument GetBaseText(string corpusName)
        {
            
            IDocument doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            if (!doc.OpenBaseText(corpusName, Helpers.GetTestUsername(), Helpers.GetTestPassword()))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }

        internal static IDocument GetVersionText(string corpusName, string versionName)
        {
            IDocument doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            if (!doc.OpenVersion(corpusName, versionName, Helpers.GetTestUsername(), Helpers.GetTestPassword()))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }       

        public static void CreateTestCorpus()
        {

            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            Assert.IsTrue(store.Open(Helpers.GetTestUsername(), Helpers.GetTestPassword()));

            HashSet<string> corpora = new HashSet<string>(store.GetCorpusList());

            if (corpora.Contains(GetTestCorpusName()))
            {
                store.DeleteCorpus(GetTestCorpusName());
            }

            store.CreateCorpus(GetTestCorpusName(), string.Empty, string.Empty);

        }

        public static string ContentStringFromFile(string filename, System.Text.Encoding encoding)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(filename, encoding);
            return sr.ReadToEnd();
        }

        public static string ContentStringFromFileWithTika(string filename, System.Text.Encoding encoding)
        {
            var bytes = File.ReadAllBytes(filename);

            var tikaExtractor = new TikaExtractor();
            var result = tikaExtractor.ConvertFileToHtml(bytes, encoding.ToString());

            return result;
        }

        public static void UploadBaseTextToTestCorpus(string filename, System.Text.Encoding encoding)
        {
            ICorpus corpus = GetCorpus(GetTestCorpusName());

            string html = ContentStringFromFile(filename, encoding);

            HtmlErrors errors = corpus.UploadBaseText(html);

            if (errors.HtmlParseErrors.Length > 0)
                throw new Exception("Errors were encountered parsing the HTML file.");

        }

        public static void UploadBaseTextToTestCorpusWithTika(string filename, System.Text.Encoding encoding)
        {
            ICorpus corpus = GetCorpus(GetTestCorpusName());

            string html = ContentStringFromFileWithTika(filename, encoding);

            HtmlErrors errors = corpus.UploadBaseText(html);

            if (errors.HtmlParseErrors.Length > 0)
                throw new Exception("Errors were encountered parsing the HTML file.");

        }
        public static void UploadVersionToTestCorpus(string versionName, string filename, System.Text.Encoding encoding)
        {
            ICorpus corpus = GetCorpus(GetTestCorpusName());

            string html = ContentStringFromFile(filename, encoding);

            HtmlErrors errors = corpus.UploadVersion(versionName, html);

            if (errors.HtmlParseErrors.Length > 0)
                throw new Exception("Errors were encountered parsing the HTML file.");

        }


        public static string GetTestDataDir(TestContext testContext)
        {
            return System.IO.Path.Combine(testContext.DeploymentDirectory, @"..\..\..\Ebla.Tests\TestData");

        }

        public static string DocContentToHtmlDoc(string content)
        {
            return "<html><head></head><body>" + content + "</body></html>";
        }


        public static string DocContentToXmlDoc(string content)
        {
            return "<?xml version=\"1.0\" ?><body>" + content + "</body>";
        }

        public static List<TestDocument> TestDocs = new List<TestDocument>()
        {
            new TestDocument("gnu2007-de-short.html", System.Text.Encoding.UTF7),
            new TestDocument("gnu2007-de.html", System.Text.Encoding.UTF7),
            new TestDocument("gnu2007-en.html", System.Text.Encoding.UTF7),
            new TestDocument("WebElements.htm", System.Text.Encoding.ASCII),
            new TestDocument("I.iii-Shakespeare.html", System.Text.Encoding.ASCII),
            new TestDocument("I.iii-Swaczynna.html", System.Text.Encoding.ASCII),
            new TestDocument("StandardDeviation.html", System.Text.Encoding.UTF8),
            new TestDocument("ZagorskaHeart.htm", System.Text.Encoding.Unicode),
            new TestDocument("result.html", System.Text.Encoding.Unicode),
            new TestDocument("example.docx", System.Text.Encoding.UTF8),
        };

        public class TestDocument
        {
            public TestDocument(string filename, System.Text.Encoding encoding) { Filename = filename; FileEncoding = encoding; }
            public string Filename { get; set; }
            public System.Text.Encoding FileEncoding { get; set; }
        }
    }
}
