﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using EblaImpl;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ebla.Tests.Unit
{
    [TestClass]
    public class EblaHelpersTests
    {

        [TestMethod]
        public void ShouldCalculateFactorialOf1()
        {
            var expected = Math.Exp(EblaHelpers.Factorial(1));
            Assert.AreEqual(1.0f, expected, 0.01f, "Factorial calculation was not successful.");
        }

        [TestMethod]
        public void ShouldCalculateFactorialOf2()
        {
            var expected = Math.Exp(EblaHelpers.Factorial(2));
            Assert.AreEqual(2.0f, expected, 0.01f, "Factorial calculation was not successful.");
        }

        [TestMethod]
        public void ShouldCalculateFactorialOf6()
        {
            var expected = Math.Exp(EblaHelpers.Factorial(3));
            Assert.AreEqual(6.0f, expected, 0.01f, "Factorial calculation was not successful.");
        }

        [TestMethod]
        public void ShouldCalculateFactorialOf24()
        {
            var expected = Math.Exp(EblaHelpers.Factorial(4));
            Assert.AreEqual(24.0f, expected, 0.01f, "Factorial calculation was not successful.");
        }

        [TestMethod]
        public void ShouldCalculatePoissonDistribution()
        {
            Assert.AreEqual(0.6065f, Math.Exp(-EblaHelpers.PoissonDistribution(0.5f, 0)), 0.0001f, "Poisson distribution was not successful.");
            Assert.AreEqual(0.3679f, Math.Exp(-EblaHelpers.PoissonDistribution(1.0f, 0)), 0.0001f, "Poisson distribution was not successful.");
            Assert.AreEqual(0.3679f, Math.Exp(-EblaHelpers.PoissonDistribution(1.0f, 1)), 0.0001f, "Poisson distribution was not successful.");
            Assert.AreEqual(0.1839f, Math.Exp(-EblaHelpers.PoissonDistribution(1.0f, 2)), 0.0001f, "Poisson distribution was not successful.");
            Assert.AreEqual(0.2707f, Math.Exp(-EblaHelpers.PoissonDistribution(2.0f, 1)), 0.0001f, "Poisson distribution was not successful.");
            Assert.AreEqual(0.1805f, Math.Exp(-EblaHelpers.PoissonDistribution(2.0f, 3)), 0.0001f, "Poisson distribution was not successful.");
        }
    }
}
