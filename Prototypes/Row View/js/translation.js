function Translation( id, url, content) {
    this.alias = 'txt_' + id;
    this.a = '#txt_'+ id;
    this.currentScrollPos = 0;
    this.activeItem = '#txt_'+id+'_id_1';
    this.lastActiveTextItem = null;
    this.scrollTarget = 0;
    this.id = id;
    this.url = url;

    content.append('<div id="'+ this.alias +'" class="txt"></div>');    

    /*this.loadTxtAndEdit = function(url, editText) { 
        $(this.a).load(url, function() {
            editText.editText();
        }); 
    }*/

    /*this.loadTxtAndFilter = function(url, filterText) {

    }*/

    this.setAlias = function(alias){ this.alias = alias; }
    this.setA = function(a){ return this.a = a; }
    this.setCurrentScrollPos = function(cs){ this.currentScrollPos = cs; }
    this.setActiveItem = function(ai){ this.activeItem = ai; }
    this.setLastActiveTextItem = function(lai){ this.lastActiveTextItem = lai; }
    this.setScrollTarget = function(st){ this.scrollTarget = st; }

    this.getAlias = function(){ return this.alias; }
    this.getA = function(){ return this.a; }
    this.getCurrentScrollPos = function(){ return this.currentScrollPos; }
    this.getActiveItem = function(){ return this.activeItem; }
    this.getLastActiveTextItem = function(){ return this.lastActiveTextItem; }
    this.getScrollTarget = function(){ return this.scrollTarget; }

    //translation[ this.id ].loadTxtAndFilter(url, translation[ id ]);
    $(this.a).load(url, function(responseText, statusText, xhr) { 
        translation[ id ].createFilter( id ); 
    });

}

/*Translation.prototype.editText = function() {
        // find all divs and save in array
        var speeches = $( this.getA() ).find('.speech'); 
        var speechesCount = speeches.length;

        // add for every text item a filterigation item
        for(var i = 0; i <= speechesCount -1 ; i++) {
        
            // add a unique ID for every item
            $(speeches[i]).attr('id', this.alias + '_' + 'id_'+ i );

        }
}

Translation.prototype.getActs = function() {
        //should be later generative
        structure = new Array();
        structure[0] = 1000; //Act1 with 1000 characters


}*/