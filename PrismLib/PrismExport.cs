﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;

namespace PrismLib
{
    public class PrismExport
    {
        public static string SanitiseFilename(string fname)
        {
            var badchars = System.IO.Path.GetInvalidFileNameChars();
            foreach (var c in badchars)
            {
                string s = string.Empty + c;
                fname = fname.Replace(s, string.Empty);
            }

            return fname;
        }


        public static void ExportCorpus(System.Xml.XmlWriter writer, ICorpus corpus, IDocument baseText, 
            string[] versions, string CorpusName, VariationMetricTypes metricType, 
            Func<string, string, IDocument> versionGetter,
            Func<string, string, IAlignmentSet> alignmentSetGetter)
        {

            writer.WriteStartElement("eblacorpus");
            writer.WriteAttributeString("name", CorpusName);
            writer.WriteAttributeString("description", corpus.GetDescription());

            writer.WriteStartElement("documents");


            ExportDoc(baseText, writer, null, string.Empty);

            foreach (var version in versions)
            {
                var versionDoc = versionGetter(CorpusName, version);

                var alignmentSet = alignmentSetGetter(CorpusName, version);

                var alignments = alignmentSet.FindAlignments(null, null, 0, baseText.Length(), 0, versionDoc.Length());


                ExportDoc(versionDoc, writer, alignments, version);
            }


            writer.WriteEndElement(); // documents

            //var stats = ChartsController.CreateEddyOverviewChartData(CorpusName, new List<string>( versions), metricType, null);

            var data = corpus.RetrieveMultipleSegmentVariationData(null, null, metricType, new List<string>(versions), false);

            writer.WriteStartElement("vivvalues");

            for (int i = 0; i < data.BaseTextSegmentIDs.Length; i++)
            {
                writer.WriteStartElement("vivvalue");
                writer.WriteAttributeString("BaseTextSegId", data.BaseTextSegmentIDs[i].ToString());
                writer.WriteAttributeString("viv", data.VivValues[i].ToString());
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); // vivvalues

            writer.WriteStartElement("eddyvalues");
            for (int j = 0; j < data.VersionNames.Length; j++)
            {
                writer.WriteStartElement("valueset");
                writer.WriteAttributeString("name", data.VersionNames[j]);
                for (int i = 0; i < data.BaseTextSegmentIDs.Length; i++)
                {
                    writer.WriteStartElement("eddyvalue");
                    writer.WriteAttributeString("BaseTextSegId", data.BaseTextSegmentIDs[i].ToString());
                    writer.WriteAttributeString("eddy", data.EddyValues[j][i].ToString());
                    writer.WriteEndElement();
                }

                writer.WriteEndElement(); // valueset

            }
            writer.WriteEndElement(); // eddyvalues


            var predefs = corpus.GetPredefinedSegmentAttributes(false);

            writer.WriteStartElement("predefinedattribs");

            foreach (var p in predefs)
            {
                writer.WriteStartElement("predefinedattrib");

                writer.WriteAttributeString("id", p.ID.ToString());
                writer.WriteAttributeString("type", p.AttributeType.ToString());
                writer.WriteAttributeString("name", p.Name);
                writer.WriteAttributeString("showintoc", p.ShowInTOC.ToString());
                writer.WriteStartElement("predefinedattribvalues");
                if (p.Values != null)
                    foreach (var v in p.Values)
                    {
                        writer.WriteStartElement("predefinedattribvalue");
                        writer.WriteAttributeString("value", v.Value);
                        writer.WriteAttributeString("applycolouring", v.ApplyColouring.ToString());
                        writer.WriteAttributeString("colourcode", v.ColourCode.ToString());
                        writer.WriteEndElement(); // predefinedattribvalue
                    }
                writer.WriteEndElement(); // predefinedattribvalues
                writer.WriteEndElement(); // predefinedattrib
            }

            writer.WriteEndElement(); // predefinedattribs

            writer.WriteEndElement(); // eblacorpus

            //writer.Flush();
            //sw.Flush();
            writer.Close();
        }

        static void ExportDoc(IDocument doc, System.Xml.XmlWriter writer, SegmentAlignment[] alignments, string name)
        {
            writer.WriteStartElement("document");
            writer.WriteAttributeString("name", name);
            var m = doc.GetMetadata();
            writer.WriteAttributeString("authortranslator", m.AuthorTranslator);
            writer.WriteAttributeString("copyrightinfo", m.CopyrightInfo);
            writer.WriteAttributeString("description", m.Description);

            writer.WriteAttributeString("genre", m.Genre);
            writer.WriteAttributeString("information", m.Information);
            writer.WriteAttributeString("langcode", m.LanguageCode);
            writer.WriteAttributeString("referencedate", m.ReferenceDate.ToString());

            writer.WriteStartElement("doccontent");

            writer.WriteRaw(doc.GetDocumentContent(0, doc.Length(), true, false, null, true, null, null, null));

            writer.WriteEndElement(); // doccontent


            var segdefs = doc.FindSegmentDefinitions(0, doc.Length(), false, false, null, null);

            writer.WriteStartElement("segmentdefinitions");

            foreach (var segdef in segdefs)
            {
                writer.WriteStartElement("segmentdefinition");

                writer.WriteAttributeString("id", segdef.ID.ToString());
                writer.WriteAttributeString("startpos", segdef.StartPosition.ToString());
                writer.WriteAttributeString("length", segdef.Length.ToString());
                writer.WriteAttributeString("contentbehaviour", segdef.ContentBehaviour.ToString());

                writer.WriteStartElement("segmentattributes");

                if (segdef.Attributes != null)
                    foreach (var attrib in segdef.Attributes)
                    {
                        writer.WriteStartElement("segmentattribute");
                        writer.WriteAttributeString("attribname", attrib.Name);
                        writer.WriteAttributeString("attribval", attrib.Value);
                        writer.WriteEndElement(); // segmentattribute
                    }

                writer.WriteEndElement(); // segmentattributes

                writer.WriteEndElement(); // segmentdefinition
            }

            writer.WriteEndElement(); // segmentdefinitions

            writer.WriteStartElement("alignments");
            if (alignments != null)
            {
                foreach (var alignment in alignments)
                {
                    writer.WriteStartElement("alignment");

                    writer.WriteAttributeString("id", alignment.ID.ToString());
                    writer.WriteAttributeString("notes", alignment.Notes);
                    writer.WriteAttributeString("status", alignment.AlignmentStatus.ToString());
                    //writer.WriteAttributeString("type", alignment.AlignmentType.ToString());

                    System.Text.StringBuilder sbids = new System.Text.StringBuilder();
                    foreach (var id in alignment.SegmentIDsInBaseText)
                    {
                        if (sbids.Length > 0)
                            sbids.Append(",");
                        sbids.Append(id.ToString());
                    }
                    writer.WriteAttributeString("BaseTextSegIds", sbids.ToString());
                    sbids = new System.Text.StringBuilder();
                    foreach (var id in alignment.SegmentIDsInVersion)
                    {
                        if (sbids.Length > 0)
                            sbids.Append(",");
                        sbids.Append(id.ToString());
                    }
                    writer.WriteAttributeString("VersionSegIds", sbids.ToString());
                    writer.WriteEndElement(); // alignment
                }

            }
            writer.WriteEndElement(); // alignments

            writer.WriteEndElement(); // document
        }

    }
}
